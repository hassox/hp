defmodule Hp.Repo do
  use Ecto.Repo,
    otp_app: :hp,
    adapter: Ecto.Adapters.Postgres
end
