# Since configuration is shared in umbrella projects, this file
# should only configure the :hp application itself
# and only for organization purposes. All other config goes to
# the umbrella root.
use Mix.Config

# Configure your database
config :hp, Hp.Repo,
  username: "postgres",
  password: "postgres",
  database: "hp_dev",
  hostname: "localhost",
  pool_size: 10
