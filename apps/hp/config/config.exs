# Since configuration is shared in umbrella projects, this file
# should only configure the :hp application itself
# and only for organization purposes. All other config goes to
# the umbrella root.
use Mix.Config

config :hp,
  ecto_repos: [Hp.Repo]

import_config "#{Mix.env()}.exs"
