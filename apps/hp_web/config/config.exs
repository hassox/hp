# Since configuration is shared in umbrella projects, this file
# should only configure the :hp_web application itself
# and only for organization purposes. All other config goes to
# the umbrella root.
use Mix.Config

# General application configuration
config :hp_web,
  ecto_repos: [Hp.Repo],
  generators: [context_app: :hp, binary_id: true]

# Configures the endpoint
config :hp_web, HpWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "9cLSHrbDTiVv1I/fsIWPMGbNZzoEkAbNHnEmnMJPnttNbVY+sAAuGkkBqrhmIcHI",
  render_errors: [view: HpWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: HpWeb.PubSub, adapter: Phoenix.PubSub.PG2]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
