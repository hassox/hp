defmodule HpWeb.PageController do
  use HpWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
